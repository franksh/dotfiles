;; -*- lexical-binding: t; -*-

;; (load (locate-user-emacs-file "init-setup.el"))
;; (load (locate-user-emacs-file "init-code.el"))
;; (load (locate-user-emacs-file "init-keybinds.el"))
;; (load (locate-user-emacs-file "init-ux.el"))
;; (load (locate-user-emacs-file "init-programming.el"))
;; (load (locate-user-emacs-file "init-org.el"))
;; (load (locate-user-emacs-file "init-tangled.el")) ; from Org file.

(require 'org-install)
;; (require 'ob-tangle)

;; potential bug: org-babel-load-file seems to always want to load the file <name>.el?
(org-babel-load-file (expand-file-name (concat user-emacs-directory "fsh-emacs.org")))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(nim-mode zig-mode julia-mode company-capf lsp-ui flycheck rustic yasnippet which-key use-package tree-sitter rainbow-delimiters org-bullets nhexl-mode magit lsp-pyright lsp-ivy kivy-mode ivy-rich ivy-prescient iflipb htmlize helpful helm-descbinds general free-keys edit-indirect doom-themes doom-modeline diminish cython-mode counsel-projectile company command-log-mode all-the-icons-dired ace-jump-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
