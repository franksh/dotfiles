;; First of all, set up a sane package environment.

(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ;; ("melpa-stable" . "https://stable.melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; 'use-package' is essential!

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;; Save and reload views and open files.

(use-package desktop
  :custom
  (desktop-dirname user-emacs-directory)
  (desktop-save t)
  :config
  (desktop-save-mode))

;; Various functions I've written during configuring Emacs.
;;
;; Most are pretty ad-hoc and not very flexible or robust.
;;
;; Many "inspired" by random posts on StackOverflow and so on.

(setq lexical-binding t) 		; Please.

(defun flash-fringe ()
  "Flash the fringe border."
  (interactive)
  (let ((prev-bg (face-background 'fringe)))
    (set-face-background 'fringe "sienna")
    (run-with-idle-timer
      0.3 nil (lambda () (set-face-background 'fringe prev-bg)))))

(defun switch-other-window-to-or (bufname function)
  "Switch to buffer `bufname` or run code (to create buffer)."
  (if (get-buffer bufname)
    (switch-to-buffer-other-window bufname)
    (split-window-sensibly (selected-window))
    (other-window 1)
    (funcall function)))

(defmacro definteractive (fun &rest body)
  `(defun ,fun ()
     (interactive)
     ,@body))

(definteractive visit-ielm (switch-other-window-to-or "*ielm*" 'ielm))
(definteractive visit-eshell (switch-other-window-to-or "*eshell*" 'eshell))

(definteractive switch-to-other-buffer (switch-to-buffer (other-buffer)))


(defun current-line-string ()
  "Current line in current buffer as a string."
  (let ((lb (line-beginning-position))
         (le (line-end-position)))
    (buffer-substring-no-properties lb le)))

(defun python-shell-eval-this ()
  (interactive)
  (let* (
         (expr-str (string-trim (current-line-string)))
         (pyth-res (python-shell-send-string-no-output expr-str))
         )
    (beginning-of-line-text)
    (delete-region (point) (line-end-position))
    (insert pyth-res)
    ))

(defun backward-word-carefully ()
     "Step backward one word without going over more than one non-word character.

     If word ends with multiple non-word characters before point, it
     will step over them but not the word itself."
     (interactive)
     (let ((cur (point))
           (mrk (progn (forward-word -1) (point)))
           (chk (progn (forward-word 1) (point))))
       (if (<= cur (1+ chk)) (goto-char mrk) chk)))

(defun backward-delete-word-carefully ()
     "Delete backward one word, but only delete /until/ word if it is far away.

     'Far away' here means its end is separated more than one character left of point."
     (interactive)
     (delete-region (point) (backward-word-carefully)))

(defun rename-buffer-and-file (&optional buffer)
  (interactive "b")
  (with-current-buffer (or buffer (current-buffer))
    (rename-current-buffer-file)))

(defun rename-current-buffer-file ()
  "Renames the current buffer and the file it is visiting."
  (interactive)
  (let ((old-bufname (buffer-name))
	 (old-filename (buffer-file-name)))
    (when (not (and old-filename (file-exists-p old-filename)))
      (error "Buffer '%s' is not visiting a file!" old-bufname))
    (let ((new-filename (read-file-name "New name: " old-filename)))
      (when (get-buffer new-filename)
        (error "A buffer named '%s' already exists!" new-filename))
      (when (file-exists-p new-filename)
	(error "File '%s' already exists!" new-filename))
      (rename-file old-filename new-filename 0) ; Don't overwrite existing files!
      (rename-buffer new-filename)
      (set-visited-file-name new-filename)
      (set-buffer-modified-p nil)
      (message "File '%s' successfully renamed to '%s'"
        old-bufname (file-name-nondirectory new-filename)))))

;; Let's drop the OS/2 GUI.
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)

;; Left-right margins on frames. Just a tiny little breathing room.
(set-fringe-mode '(8 . 0))

;; (tooltip-mode -1)
(setq tooltip-delay 0.2)

(set-face-attribute 'default nil :font "Fira Mono" :height 160)

(set-face-attribute 'fixed-pitch nil :font "DejaVu Sans Mono" :height 160)
(set-face-attribute 'variable-pitch nil :font "Cantarell" :height 160)

(use-package diminish)

;; UPDATE: replaced by doom theme
;; (load-theme 'tango-dark)

(use-package all-the-icons
  :init
  (unless (find-font (font-spec :name "all-the-icons"))
    (all-the-icons-install-fonts t)))

(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package doom-modeline
  :custom
  (doom-modeline-minor-modes t)
  :config
  (doom-modeline-mode 1))

(use-package doom-themes
  :config
  (setq doom-themes-enable-bold t
	doom-themes-enable-italic t)
  ;; Tolerable alternatives:
  ;; (load-theme 'doom-gruvbox)
  ;; (load-theme 'doom-sourcerer
  ;; (load-theme 'doom-opera)
  ;; (load-theme 'doom-monokai-spectrum)
  (load-theme 'doom-tomorrow-night t)
  (doom-themes-visual-bell-config)
  ;; (doom-themes-treemacs-config)
  (doom-themes-org-config))

;; Might wanna use this throughout.

(use-package general
  :demand)

(use-package free-keys)			; M-x free-keys is useful.
 
;; Helpful things.

(use-package which-key
  :diminish
  :config (which-key-mode)
  :custom (which-key-idle-delay 0.3))

(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-key] . helpful-key))

(use-package helm-descbinds) ; (helm-descbinds-mode)

;; Yet-another-alt-tab.

(use-package iflipb
  :custom
  (iflipb-ignore-buffers nil))

;; We're not French.
(setq sentence-end-double-space nil)

;; Enable line numbers ...
(global-display-line-numbers-mode)

;; ... almost everywhere.
(setq line-number-mode-exceptions
  '(term-mode-hook
     eshell-mode-hook
     shell-mode-hook
     special-mode
     org-mode-hook
     ))
				   
(dolist (m line-number-mode-exceptions)
  (add-hook m (lambda () (display-line-numbers-mode 0))))

;; We also care about offsets.
(column-number-mode)

;; (setq make-backup-files nil)

;; Don't show ugly welcome screen.
(setq inhibit-startup-message t)

(use-package yasnippet
  :diminish yas-minor-mode
  :config
  (yas-global-mode))

;; (use-package command-log-mode
;;  :config (global-command-log-mode))

;; Things that make the interactive experience better.

(use-package swiper)

(use-package ivy
  ;; :config (ivy-mode)
  :diminish
  :custom
  (ivy-initial-inputs-alist nil))

(use-package counsel
  :diminish
  :after (ivy swiper)
  :config (counsel-mode))

(use-package prescient
  :after (counsel)
  :config (prescient-persist-mode))

(use-package ivy-prescient
  :after (ivy prescient)
  :demand
  :custom
  (ivy-prescient-enable-filtering nil)
  :config (ivy-prescient-mode))

(use-package ivy-rich
  :after (ivy counsel-projectile)
  :init (ivy-rich-mode))

(use-package lsp-mode
  :init
  (setq lsp-keymap-prefix "M-l")
  :custom
  (lsp-pyls-plugins-pylint-enable nil)
  (lsp-pyls-plugins-pyflakes-enable nil)
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 0.4)
  (lsp-rust-analyzer-server-display-inlay-hints t)
  (lsp-prefer-capf t)
  ;; (lsp-rust-analyzer-server-command '("~/.rustup/ 'rust-analyzer)
  :config
  ;; (require 'lsp)
  ;; (require 'lsp-pyright)
  (lsp-enable-which-key-integration t)
  (add-hook 'lsp-mode-hook 'lsp-ui-mode)
    ;; :hook (python-mode . lsp-deferred)
    )

  (use-package lsp-ui
     :after lsp
     :custom
     (lsp-ui-peek-always-show t)
     (lsp-ui-sideline-show-hover t)
     (lsp-ui-doc-enable nil)
     )
  ;;   (lsp-ui-doc-position 'bottom)
  ;;   (lsp-ui-doc-delay 1.0)
  ;;   :hook (lsp-mode . lsp-ui-mode)
  ;;   )

(use-package lsp-ivy
  :after (ivy lsp))
(use-package company
    :diminish
  :custom
  (company-backends '(company-capf))
  :config
  (global-company-mode +1))

(setq c-basic-offset 2)
(setq tab-width 2)
(setq indent-tabs-mode nil)


(use-package magit
  ;; :custom (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))
  ;; :config
  ;; (diminish 'auto-revert-mode "")
  )

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; Needed by markdown-mode
(use-package edit-indirect)

;; Use GitHub-flavor by default.
(use-package markdown-mode
  :after edit-indirect
  :mode (("\\.md\\'" . gfm-mode)))

(use-package projectile
  :after ivy
  :custom
  (projectile-completion-system 'ivy)
  :bind-keymap
  ("C-+" . projectile-command-map)
  :init
  (setq projectile-project-search-path '("~/code"))
  (setq projectile-switch-project-action #'projectile-dired)
  (projectile-mode))

(use-package counsel-projectile
  :after (counsel projectile)
  :custom
  (counsel-projectile-sort-files t)
  (counsel-projectile-sort-directories t)
  :config (counsel-projectile-mode))

(use-package python
  :ensure nil
  ;; :hook (python-mode . lsp-deferred)
  :custom
  (python-indent-offset 2)
  (python-shell-interpreter (expand-file-name "~/env/py38/bin/python"))
  ;; (python-shell-virtualenv-root (expand-file-name "~/env/py38"))
  ;;  :bind (("M-{" . python-nav-backward-defun)
  ;;	 ("M-}" . python-nav-forward-defun))
  )

(use-package cython-mode)

(use-package org
    :custom
    (org-ellipsis " ▼▼ ")
    (org-todo-keywords '((sequence "TODO" "PROGRESS" "|" "CANCELLED" "DONE")))
    (org-startup-indented t)
    (org-indent-mode t)
    :demand
    :config
    (require 'org-tempo)
    (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
    (add-to-list 'org-structure-template-alist '("py" . "src python")))

  (use-package org-bullets
    :after org
    :hook (org-mode . org-bullets-mode))

  (require 'ox-publish) ;; unnecessary?
  (require 'ox-html)

  (use-package htmlize)

  (setq org-publish-project-alist
    '(("ctfnotes"
       :base-directory "~/onedrive/ctfnotes/"
       :base-extension "org"
       :publishing-directory "~/test/m13h.github.io/html/"
       :recursive t
       :publishing-function org-html-publish-to-html
       :section-numbers nil
       :with-date nil
       :with-author nil
       :with-timestamps nil
       :html-postamble nil
       )))


(add-to-list 'org-latex-classes
             '("fbook"
               "\\documentclass[a4paper,12pt,oneside,openany]{memoir}
\\usepackage[utf8]{inputenc}
\\usepackage[T1]{fontenc}
\\usepackage{amsmath}
\\usepackage{amssymb}
\\usepackage{hyperref}
\\usepackage{epigraph}
\\usepackage{parskip}
\\chapterstyle{dash}
\\addtopsmarks{headings}{}{
  \\createmark{chapter}{both}{shownumber}{}{. \\ }
}
\\pagestyle{headings}
\\tolerance=1000
      [NO-DEFAULT-PACKAGES]
      [PACKAGES]
      [EXTRA]
\\linespread{1.1}"
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(set-face-attribute 'org-level-1 nil :font "Cantarell" :height 1.3)
(set-face-attribute 'org-level-2 nil :font "Cantarell" :height 1.2)
(set-face-attribute 'org-level-3 nil :font "Cantarell" :height 1.1)
(set-face-attribute 'org-level-4 nil :font "Cantarell" :height 1.05)
(set-face-attribute 'org-level-5 nil :font "Cantarell" :height 1.0)

(setq flycheck-emacs-lisp-load-path 'inherit)
(use-package flycheck)
(use-package rustic
  :custom
  (rustic-indent-offset 2))

(use-package julia-mode
  :custom
  (julia-indent-offset 2))

(use-package zig-mode
  :custom
  (zig-indent-offset 2)
  (zig-format-on-save nil))

(use-package nim-mode)

(eval-after-load 'org-indent '(diminish 'org-indent-mode))
;; alternative: (add-hook 'org-indent-mode-hook (lambda () (diminish 'org-indent-mode)))

(diminish 'auto-revert-mode)

(aset text-mode-syntax-table ?_ (aref text-mode-syntax-table ?a))
(aset text-mode-syntax-table ?- (aref text-mode-syntax-table ?a))

;; So nice.

(use-package ace-jump-mode
  :bind
  ("C-æ" . ace-jump-mode)
  ("M-æ" . ace-jump-mode-pop-mark))

;; Because I like to have all the keybindings in one place.

;; Lisp note: it's OK to bind keys to 'SYMBOL before loading the
;; package that provides it. The symbol's function cell is looked up
;; later.


;; FUCK THIS KEY.

(general-define-key
  "C-x C-z" nil)

(general-create-definer
 fsh/keys-m-r
 :prefix "M-r")

(fsh/keys-m-r
  "" nil
  "s" 'visit-eshell
  "e" 'visit-ielm
  "p" 'run-python)

(general-define-key
 :keymaps 'org-mode-map
 "<RET>" 'org-return-indent
 )

(general-define-key
  ;; Nice convenient binds.
  "M-b" 'switch-to-other-buffer		; BEST BIND IN THE WORLD.
  "C-S-k" 'kill-this-buffer
  "C-z" 'undo
  "C-o" 'other-window

  ;; UX interface. (Not sure if set automatically or not?)
  "C-s" 'counsel-grep-or-swiper
  "C-S-s" 'counsel-grep-or-swiper-backward
  "C-b" 'counsel-switch-buffer
  "C-S-b" 'counsel-switch-buffer-other-window

  [(meta shift q)] 'iflipb-previous-buffer
  [(meta shift f)] 'iflipb-next-buffer

  "C-h C-f" 'find-function
  "C-h b" 'helm-descbinds

  ;; I never use C-<digit> anyway.
  "C-0" 'delete-window
  "C-1" 'delete-other-windows
  "C-2" 'split-window-below
  "C-3" 'split-window-right

  ;; Rebinds.
  "M-{" 'backward-paragraph
  "M-}" 'forward-paragraph
  "M-[" 'backward-sentence
  "M-]" 'forward-sentence
  "M-(" 'backward-word
  "M-)" 'forward-word

  ;; I have ↑←→↓ bound comfortably close to home row with AltGr.
  "C-<left>" 'backward-word
  "C-<right>" 'forward-word
  "C-<up>" 'backward-sentence
  "C-<down>" 'forward-sentence
  "M-<left>" 'backward-paragraph
  "M-<right>" 'forward-paragraph
  "M-<up>" 'scroll-down-command
  "M-<down>" 'scroll-up-command

  ;; Experimental
  "C-S-e" 'python-shell-eval-this
  "C-∅" 'helpful-kill-buffers
  "C-x g" 'magit-status
  [?\M-\d] 'backward-delete-word-carefully)

(general-define-key
  :keymaps 'compilation-mode-map
  "C-o" 'other-window
  "M-o" 'compilation-display-error)
