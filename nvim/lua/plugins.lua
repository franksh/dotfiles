local _utrlyr =  function()
  -- Does cmp respect this? It doesn't seem that way.
  vim.opt.completeopt = { 'noinsert', 'menu', 'menuone', 'noselect' }


  local lspkind = require('lspkind').init({ preset = 'default', })


  --- Learn the keybindings, see :help lsp-zero-keybindings
  -- Learn to configure LSP servers, see :help lsp-zero-api-showcase
  local lsp = require('lsp-zero').preset('recommended')

  lsp.set_preferences({
    set_lsp_keymaps = { omit = { 'K' } }, -- I use K->I mapping.
  })
  lsp.on_attach(function()
    vim.opt.formatexpr = nil
  end)

  -- local cmp_config = lsp.defaults.cmp_config({
    lsp.setup_nvim_cmp({
      formatting = {
        format = function(...)
          return require("lspkind").cmp_format({ mode = "symbol_text" })(...)
        end,
      },
      completion = {
        -- it needs to be here to be respected?
        completeopt = 'menu,menuone,noinsert,noselect',
      },
      sources = {
        { name = "luasnip", priority = 100 },
        { name = "nvim_lsp", priority = 90 },
        { name = "nvim_lsp_signature_help" },
        { name = "nvim_lua", priority = 80 },
        { name = "copilot", priority = 70 },
        { name = "path", priority = 10 },
        { name = "buffer", priority = 1, keyword_length = 5 },
      },
    })

    -- (Optional) Configure lua language server for neovim
    lsp.nvim_workspace()

    -- local luasnip = require("luasnip")
    local cmp = require("cmp")
    -- cmp.setup({
      --   ['<CR>'] = cmp.mapping(function(fallback)
        --         if cmp.visible() then
        --             if luasnip.expandable() then
        --                 luasnip.expand()
        --             else
        --                 cmp.confirm({
          --                     select = true,
          --                 })
          --             end
          --         else
          --             fallback()
          --         end
          --     end),

          --     ["<Tab>"] = cmp.mapping(function(fallback)
            --       if cmp.visible() then
            --         cmp.select_next_item()
            --       elseif luasnip.locally_jumpable(1) then
            --         luasnip.jump(1)
            --       else
            --         fallback()
            --       end
            --     end, { "i", "s" }),

            --     ["<S-Tab>"] = cmp.mapping(function(fallback)
              --       if cmp.visible() then
              --         cmp.select_prev_item()
              --       elseif luasnip.locally_jumpable(-1) then
              --         luasnip.jump(-1)
              --       else
              --         fallback()
              --       end
              --     end, { "i", "s" }),
              -- })

              cmp.setup.cmdline(":", {
                completion = { completeopt = 'noinsert,menu,menuone,noselect', },
                mapping = cmp.mapping.preset.cmdline(),
                sources = cmp.config.sources({
                  { name = "path" },
                  { name = "nvim_lua" },
                }, {
                  {
                    name = "cmdline",
                    option = {
                      ignore_cmds = { "Man", "!" },
                    },
                  },
                }),
              })
              cmp.setup.cmdline("/", {
                completion = { completeopt = 'menu,menuone,noselect', },
                mapping = cmp.mapping.preset.cmdline(),
                sources = {
                  { name = "buffer" },
                },
              })

              lsp.configure('pyright', {
              })

              lsp.configure('nimlangserver', {
                settings = {
                  nim = {}
                }
              })

              -- so fickle / hard to get this to work for some reason.
              lsp.configure('pylsp', {
                on_attach = function()
                  -- plzzz
                  -- unbalanced parenthesis inside strings are broken. somehow (why??????????) this fixes it.
                  --
                  -- like '('<cr>
                  --
                  -- frail frail frail
                  vim.opt_local.syntax = 'python'
                end,
                settings = {
                  pylsp = {
                    configurationSources = {'pylint'},
                    plugins = {
                      pycodestyle = {enabled = false},
                      pyflakes = {enabled = false},
                      flake8 = {
                        enabled = false,
                        ignore = {},
                      },
                      pylint = {
                        enabled = true,
                        executable = "pylint",
                      }
                    }
                  },
                  pyright = {},
                }
              })

              local lspconfig = require('lspconfig')
              lspconfig.pyright.setup({})

              lsp.setup()
            end

return {
  -- Git.
  { 'TimUntersberger/neogit' };

  -- Undo tree.
  { 'mbbill/undotree',
    config = function()
      setkeys({{'L', vim.cmd.UndotreeToggle},})
    end,
  };

  -- Motions and objects.
  -- 'easymotion/vim-easymotion';
  -- 'rhysd/clever-f.vim';
  'machakann/vim-sandwich';
  -- { 'ggandor/lightspeed.nvim',
  --   dependencies = { 'machakann/vim-sandwich' },
  --   opts = {},
  -- };

  -- this one messes with keybindings and has no customization
  -- 'michaeljsmith/vim-indent-object';
  -- c.f. treesitter-textobjects
  'mizlan/iswap.nvim';

  -- Hydra.
  'anuvyklack/hydra.nvim';

  -- Plug 'MattesGroeger/vim-bookmarks
  {
    'ThePrimeagen/harpoon',
    config = function(opts)
      local mark = require('harpoon.mark')
      local ui = require('harpoon.ui')
      setkeys({
        { '<leader>m', mark.add_file, desc='Harpoon file' },
        { '∅', ui.toggle_quick_menu, desc='Harpoon menu' },
        { '§', bind(ui.nav_file, 1), desc='File 1' },
        { '¶', bind(ui.nav_file, 2), desc='File 2' },
        { '£', bind(ui.nav_file, 3), desc='File 3' },
        { '¤', bind(ui.nav_file, 4), desc='File 4' },
        { '±', bind(ui.nav_file, 5), desc='File 5' },
        { '∂', bind(ui.nav_file, 6), desc='File 6' },
      })
    end,
  };

  -- Misc.
  'tpope/vim-commentary';
  'chrisbra/unicode.vim';
  'chrisbra/recover.vim'; -- do i use this anymore?
  -- 'junegunn/vim-easy-align';
  'andymass/vim-matchup';
  'echasnovski/mini.nvim';
  { 'echasnovski/mini.align',
    opts = {
      -- Module mappings. Use `''` (empty string) to disable one.
      mappings = {
        start = 'gl',
        start_with_preview = 'gL',
      },
    },
  };

  -- Visuals.
  'luochen1990/rainbow';
  'Shatur/neovim-ayu';

  {
    'morhetz/gruvbox',
    config = function()
      local ayu = require('ayu')
      ayu.setup({
        mirage = false,
        overrides = {},
      })
      ayu.colorscheme()
    end,
  };


  -- 'windwp/nvim-autopairs';

  -- Dev & Modes.
  'bfredl/nvim-luadev';
  'habamax/vim-asciidoctor';
  'alaviss/nim.nvim';
  'Glench/Vim-Jinja2-Syntax';

  -- TODO: iron
}



  --
  -- Old stuff from old configs.
  -- Plug 'tpope/vim-vinegar';

  -- 'antoinemadec/FixCursorHold.nvim';
  --
  -- claims to be fast but isn't
  -- 'vim-airline/vim-airline';
  -- 'vim-airline/vim-airline-themes';

  -- 'Vimjas/vim-python-pep8-indent';

  -- " Nim.
  -- Plug 'scrooloose/syntastic';
  -- Plug 'zah/nim.vim';

  -- " 'Always load devicons last' it says...
  -- Plug 'ryanoasis/vim-devicons';

  -- Plug 'lark-parser/vim-lark-syntax';
  -- Plug 'ThePrimeagen/vim-be-good';
  -- Plug 'mg979/vim-xtabline';
  -- Plug 'itchyny/lightline.vim';
  --
  -- " Help-related stuff becaI am dumb and forget things.
  -- Plug 'folke/which-key.nvim';
  -- Plug 'sudormrfbin/cheatsheet.nvim';

  -- " Snippets.
  -- Plug 'SirVer/ultisnips';
  -- Plug 'honza/vim-snippets';

