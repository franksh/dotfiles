local function config(plugin, opts)
  require("luasnip.loaders.from_vscode").lazy_load()

  require("luasnip.loaders.from_lua").load({
    paths = { "./luasnippets" },
  })
end

return {
  'L3MON4D3/LuaSnip',
  name = 'luasnip',
  dependencies = {
    {'rafamadriz/friendly-snippets' },
  },
  config = config,
}
