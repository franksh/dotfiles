local function note_opener(prompt_bufnr, map)
  local actions = require('telescope.actions')
  local action_set = require('telescope.actions.set')
  local action_state = require('telescope.actions.state')

  local new_note = function(line)
    local ext = vim.fn.fnamemodify(line, ':e')
    if not vim.tbl_contains({'md', 'adoc', 'txt', 'rst', 'doc'}, ext) then
      line = line .. '.adoc'
    end
    vim.api.nvim_command('$tabe ~/MEGA/notes/' .. line)
  end

  map({'i', 'n'}, '<c-cr>', function()
    local line = action_state.get_current_line()
    actions.close(prompt_bufnr)
    new_note(line)
  end)

  action_set.select:replace(function()
    local entry = action_state.get_selected_entry()
    local line = action_state.get_current_line()
    actions.close(prompt_bufnr)

    if entry then
      vim.cmd.tabe(entry.path)
    else
      new_note(line)
    end
  end)
  return true
end

local function config(plugin, opts)
  require('telescope').setup({
    defaults = require('telescope.themes').get_ivy({
      path_display = 'smart',
      winblend = 15,
      layout_config = {
        height = 0.8,
      },
      mappings = {
        i = {
          ["<c-h>"] = "which_key",
        },
      },
    }),
  })
  require("telescope").load_extension('harpoon')
  require('telescope').load_extension('luasnip')

  local builtin = require('telescope.builtin')

  setkeys({
    mode = 'n',
    { '<leader>ff', builtin.find_files, desc = 'Find files' },
    { '<leader>fg', builtin.git_files, desc = 'Git files' },
    { '<leader>fs', builtin.live_grep, desc = 'Live grep' },

    { '<leader><space>', builtin.buffers, desc = "Buffers..." },

    { '<leader>/', bind(builtin.current_buffer_fuzzy_find, { previewer = false }), desc = "Fuzzy find in buffer" },

    { '<leader>ft', builtin.treesitter, desc = 'Treesitter' },

    { '<leader>?', builtin.help_tags, desc = 'Help tags' },
    { '<leader>ro', builtin.oldfiles, desc = 'Old files' },

    { '<leader>le', builtin.diagnostics, desc = 'LSP diagnostics' },
    { '<leader>lr', builtin.lsp_references, desc = 'LSP references' },
    { '<leader>ld', builtin.lsp_definitions, desc = 'LSP definitions' },
    { '<leader>li', builtin.lsp_implementations, desc = 'LSP implementations' },
    { '<leader>lt', builtin.lsp_type_definitions, desc = 'LSP type definitions' },
    { '<leader>ls', builtin.lsp_document_symbols, desc = 'LSP symbols' },
    { '<leader>lw', builtin.lsp_workspace_symbols, desc = 'LSP workspace symbols' },

    { '<leader>fk', builtin.keymaps, desc = "Keybindings" },

    { '<leader>j', bind(builtin.jumplist, { fname_width = 40, path_display = { 'smart', }, layout_config = { preview_width = 0.4, }, }), desc = 'Treesitter', },

    { '<leader>n', function()
      builtin.find_files({
        prompt_title = "Notes",
        cwd = '~/MEGA/notes',
        attach_mappings = note_opener,
      })
    end, desc = "Notes..." },
  })
end

return {
  'nvim-telescope/telescope.nvim',
  name = 'telescope',
  branch = '0.1.x',
  dependencies = {
    { 'nvim-lua/plenary.nvim' },
    { 'L3MON4D3/LuaSnip' },             -- Required
    { 'benfowler/telescope-luasnip.nvim' },
    {
      'nvim-telescope/telescope-fzf-native.nvim',
      build = 'make',
      config = function()
        require("telescope").load_extension("fzf")
      end,
    },
  },
  config = config,
}

