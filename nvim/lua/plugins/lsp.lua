
local function config()
  vim.diagnostic.config({
    underline = {
      severity = { max = vim.diagnostic.severity.INFO }
    },
    virtual_text = {
      severity = { min = vim.diagnostic.severity.WARN }
    }
  })

  vim.api.nvim_create_autocmd('LspAttach', {
    callback = function(event)
      local function opts(d)
        return { buffer = event.buf, desc = d }
      end
      local km = vim.keymap

      km.set('n', '<leader>h', function() vim.lsp.buf.hover() end, opts('LSP Hover'))

      -- km.set('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', opts)
      km.set('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>', opts('Definition (LSP)'))
      km.set('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>', opts('Declaration (LSP)'))
      km.set('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<cr>', opts('Implementation (LSP)'))
      km.set('n', 'go', '<cmd>lua vim.lsp.buf.type_definition()<cr>', opts('Type definition (LSP)'))
      km.set('n', 'gr', '<cmd>lua vim.lsp.buf.references()<cr>', opts('References (LSP)'))
      km.set('n', 'gs', '<cmd>lua vim.lsp.buf.signature_help()<cr>', opts('Signature help (LSP)'))
      km.set('n', '<leader>lR', '<cmd>lua vim.lsp.buf.rename()<cr>', opts('Rename'))
      km.set({'n', 'x'}, '<leader>lF', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', opts('LSP format'))
      km.set('n', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>', opts('LSP Declaration'))
      km.set('n', '<leader>lc', function() vim.lsp.buf.code_action() end, opts('LSP code actions'))
    end,
  })


  local lspconfig_defaults = require('lspconfig').util.default_config
  lspconfig_defaults.capabilities = vim.tbl_deep_extend(
    'force',
    lspconfig_defaults.capabilities,
    require('cmp_nvim_lsp').default_capabilities()
  )

  local setups = {
    ts_ls = {
      on_attach = function(client, bufnr)
        -- vim.notify('CAPS :: ' .. vim.inspect(client.server_capabilities))
        -- vim.notify('CONFIG :: ' .. vim.inspect(client.config))
        -- Disable unwanted features
        -- client.server_capabilities.document_formatting = false
        -- client.server_capabilities.document_range_formatting = false

        -- -- You can add custom keymaps here specific to TypeScript if needed
        -- -- (e.g., Go to Definition is already bound globally as `gd`)

        -- -- You can also define settings to suppress unwanted diagnostics
        -- client.config.flags = client.config.flags or {}
        -- client.config.flags.debounce_text_changes = 150
      end,
    },

    lua_ls = {
      settings = {
        Lua = {
          workspace = {
            library = vim.api.nvim_get_runtime_file("lua", true),
            checkThirdParty = false,
          },
        },
      }
    },
  }

  require('mason').setup({})
  require('mason-lspconfig').setup({
    handlers = {
      function(server_name)
        -- vim.print("setting up server " .. server_name)
        require('lspconfig')[server_name].setup(setups[server_name] or {})
      end,
    }
  })

  local cmp = require('cmp')
  cmp.setup({
    completion = {
      completeopt = 'menu,menuone,noinsert,noselect',
    },
    sources = {
      {name = 'path'},
      {name = 'nvim_lsp'},
      {name = 'nvim_lua'},
      {name = 'luasnip', keyword_length = 2},
      {name = 'buffer', keyword_length = 3},
    },
    window = {
      completion = cmp.config.window.bordered(),
      documentation = cmp.config.window.bordered(),
    },
    snippet = {
      expand = function(args)
        require('luasnip').lsp_expand(args.body)
      end,
    },
    mapping = cmp.mapping.preset.insert({
      -- confirm completion item
      ['<Enter>'] = cmp.mapping.confirm({ select = true }),

      -- trigger completion menu
      ['<C-Space>'] = cmp.mapping.complete(),

      -- scroll up and down the documentation window
      ['<C-u>'] = cmp.mapping.scroll_docs(-4),
      ['<C-d>'] = cmp.mapping.scroll_docs(4),   

      -- jump to the next snippet placeholder
      ['<C-f>'] = cmp.mapping(function(fallback)
        local luasnip = require('luasnip')
        if luasnip.locally_jumpable(1) then
          luasnip.jump(1)
        else
          fallback()
        end
      end, {'i', 's'}),

      -- jump to the previous snippet placeholder
      ['<C-b>'] = cmp.mapping(function(fallback)
        local luasnip = require('luasnip')
        if luasnip.locally_jumpable(-1) then
          luasnip.jump(-1)
        else
          fallback()
        end
      end, {'i', 's'}),
    }),
  })

  cmp.setup.cmdline(":", {
    completion = { completeopt = 'noinsert,menu,menuone,noselect', },
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
      { name = "path" },
      { name = "nvim_lua" },
    }, {
      {
        name = "cmdline",
        option = {
          ignore_cmds = { "Man", "!" },
        },
      },
    }),
  })
  cmp.setup.cmdline("/", {
    completion = { completeopt = 'menu,menuone,noselect', },
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
      { name = "buffer" },
    },
  })
end

return {
    'neovim/nvim-lspconfig',
    config = config,
    dependencies = {
      {'williamboman/mason.nvim'},
      {'williamboman/mason-lspconfig.nvim'},
      {'hrsh7th/nvim-cmp',

      {'hrsh7th/cmp-nvim-lsp'},
      {'hrsh7th/cmp-buffer'},
      {'hrsh7th/cmp-path'},
      {'hrsh7th/cmp-cmdline'},     -- Required
      {'hrsh7th/cmp-nvim-lua'},     -- Optional

      {'saadparwaiz1/cmp_luasnip'},
    },
  }
}
