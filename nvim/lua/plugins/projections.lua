
local function config(plugin, opts)
  require("projections").setup(opts)

  require('telescope').load_extension('projections')
  vim.keymap.set("n", "<leader>p", ":Telescope projections<cr>", { desc = "Projections" })

  -- Autostore session on VimExit
  local Session = require("projections.session")
  vim.api.nvim_create_autocmd({ 'VimLeavePre' }, {
    callback = function() Session.store(vim.loop.cwd()) end,
  })

  -- Switch to project if vim was started in a project dir
  local switcher = require("projections.switcher")
  vim.api.nvim_create_autocmd({ "VimEnter" }, {
    callback = function()
      if vim.fn.argc() == 0 then switcher.switch(vim.loop.cwd()) end
    end,
  })
end

local setts = {
  'GnikDroy/projections.nvim',
  name = 'projections',
  branch = 'pre_release',
  dependencies = { 'nvim-tree/nvim-tree.lua', 'telescope' },
  config = config,
  opts = {
    workspaces = {
      '~/dev',
      '~/code',
      '~/git',

      '~/dev/js',
      '~/dev/ts',
      '~/dev/python',
      '~/dev/haskell',
      '~/dev/nim',
      '~/src',
      { '~/MEGA', { 'master.adoc', 'index.adoc', '.git' }, },
      { '/home/franksh', { '.git' }, }, -- '~' not working?
    },
    patterns = { '.git', 'README.md', 'README.adoc', 'LICENSE', '.svn', '.hg', 'src' },
    store_hooks = {
      pre = function()
        require('nvim-tree').api.tree.close()
      end,
      post = function() end,
    },
    restore_hooks = {
      post = function()
        require('nvim-tree').api.tree.toggle()
        vim.cmd.wincmd('w')
      end,
      pre = function() end,
    },
  },
}

return {}
