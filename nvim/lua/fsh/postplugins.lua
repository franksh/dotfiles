
local wk = require('which-key')

local function insf(f, ...)
  local args = { ... }
  return function() vim.api.nvim_put({f(unpack(args))}, 'c', true, true) end
end

local function rootdir(f)
  local d = git.root() or vim.fn.getcwd()
  return f and vim.fs.joinpath(d, f) or d
end

wk.add({ '<leader>r', desc = 'Open...'})
wk.add({' <leader>ri', bind(vim.cmd.edit, vim.fn.stdpath('config') .. '/init.lua'), desc = "Open init.lua" })
wk.add({' <leader>rf', bind(vim.cmd.edit, vim.fn.stdpath('config') .. '/init.lua'), desc = "Open init.lua" })

wk.add({ '<leader>rk', bind(vim.cmd.edit, vim.fn.stdpath('config') .. '/lua/fsh/keys.lua'), desc = "Key bindings" })
wk.add({ '<leader>rK', bind(vim.cmd.edit, vim.fn.stdpath('config') .. '/lua/fsh/postplugins.lua'), desc = "Keys (postplugins)" })
wk.add({ '<leader>rP', bind(vim.cmd.edit, vim.fn.stdpath('config') .. '/lua/plugins.lua'), desc = "plugins" })

wk.add({ '<leader>rp', bind(vim.cmd.edit, vim.fn.stdpath('config') .. '/lua/plugins'), desc = "Plugins..." })
wk.add({ '<leader>ri', function() vim.cmd.edit(rootdir('.gitignore')) end, desc = "Open .gitignore" })

setkeys({
  { '<leader>id', insf(vim.fn.strftime, '%Y-%d-%m'), desc = "Date <YY-dd-mm>" },
  { '<leader>it', insf(vim.fn.strftime, '%H-%M-%S'), desc = "Time <HH:MM:SS>" },
  { '<leader>i.', insf(vim.fn.getcwd), desc = "CWD" },
})
