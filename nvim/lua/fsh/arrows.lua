

function chch(s)
  local res = {}
  for c in iterstr(s) do
    table.insert(res, c)
  end
  return res
end





local maxlen
function maxlen(arr)
  return fold(function(x, y) return #x < #y and #y or #x end, 0, arr)
end


function place(s, i, what)
  if i > #s then
    s = s .. (" "):rep(i - #s)
  end
  return s:sub(1, i - 1) .. what .. s:sub(i + #what)
end


function draw(col, ...)
  for i = low + 1, high - 1 do
    local line = lines[i]
    if pts[i] then
      place(i, pts[i].start, pts[i].tip)
      for j = pts[i].start + 1, col - 1 do
        place(i, j, '-')
      end
      local x = pts[i].tip .. pts[i].body:rep(col - pts[i].start - 1) .. corner
    else
      place(i, col, '|')
    end
  end
end


--local f = function()
--  return {
--    ['││ ─'] = '├',
--    ['││─ '] = '┤',
--    ['││  '] = '│',
--    ['│  ─'] = '╰',
--    ['│ ─ '] = '╯',
--    ['│─  '] = '',
--    [' │ ─'] = '╭',
--    [' │─ '] = '╮',
--    ['─│  '] = '',
--    ['  │─'] = '',
--    [' ─│ '] = '',
--    ['─ │ '] = '',
--    ['  ─│'] = '',
--    [' ─ │'] = '',
--    ['─  │'] = '',
--  }
--end
----─
--@│
--

--┏━┓
--┃@┃
--┗━┛

--╔═╗
--║@║
--╚═╝

--╒═╕
--│@│
--╰─╯
--'''


--'''
--╔═╤═╦═╤═╗
--║ │ ║ │ ║
--╟─┼─╫─┼─╢
--║ │ ║ │ ║
--╠═╪═╬═╪═╣
--║ │ ║ │ ║
--╟─┼─╫─┼─╢
--║ │ ║ │ ║
--╚═╧═╩═╧═╝






local vim_quote
-- Quote a string for use with Vim patterns/REs by surrounding it with \V...\v
--
function vim_quote(s)
  -- Question: is there a standard vim function to auto-escape a string for other modes? Or some
  -- place that lists every char special in magic/nonmagic/very-magic modes?
  --
  -- Help says very-magic means _every_ ASCII except alphanumeric and - is special but that's not
  -- true (e.g. backtick, forward slash, hash, etc.)
  local esc = vim.fn.escape(s, '\\')
  return '\\V' .. esc .. '\\v'
end





local config = {
  -- A string prefixed to all arrow drawings.
  start_string = "  ",

  -- A Vim pattern/RE that is required to match after a line with arrows on it; otherwise it will
  -- not be recognized. It's interpreted in `\v` mode by default.
  end_pattern = "$| ", -- end of line or a space

  fill = " ", -- separator for parallel arrows
  ws = " ", -- whitespace used for filling

  -- Trailing stuff to ignore. This matches comments in most languages.
  --
  -- Note: \v introduces very-magic mode, and %() is a non-capturing grouping.

  arrows = {
    ascii = {
      h = "-",
      v = "|",
      tr = ".", -- top right
      br = "'", -- bottom right
      x = '+', -- when a running arrow crosses an outgoing arrow
      sep = ' ',
      tip = '<',
      tail = '-',
    },
  }
}


local vp_alt, vp_delim
function vp_alt(...)
  return '%(' .. table.concat({...}, '|') .. ')'
end
function vp_delim(l, sep)
  return '%(' .. l .. '%(' .. sep .. l .. ')*)'
end

function build_re(arr)
  arr = map_values(arr, vim_quote)
  local start_p = vim_quote(config.start_string)
  local end_p = '%(' .. config.end_pattern .. ')'

  -- matches the start or end of a sideline minus the lane turn
  --
  -- <-------. | | |
  -- ^^^^^^^^
  local body = vp_alt(arr.tip, arr.tail) .. vp_alt(arr.x, arr.h) .. '+'
  -- matches the last part of the body where it turns into a lane
  --
  -- <-------. | | |
  --         ^
  local corner = vp_alt(arr.tr, arr.br)
  -- matches the lanes at the end
  --
  -- <-------. | | |
  --           ^^^^^
  local lanes = vp_delim(arr.v, arr.sep)

  -- matches the start/end of a sideline
  local origin = body .. '\\zs' .. corner .. vp_alt(arr.sep .. lanes) .. '?\\ze'
  -- matches a line that does not start or end a sideline but only has lanes
  local run = '\\zs' .. lanes .. '\\ze'

  return {
    endpoint_p = start_p .. origin .. end_p,
    lanes_p = start_p .. run .. end_p,
    both_p = start_p .. vp_alt(origin, run) .. end_p,
  }
end

local sl = {}

function sl.endpoint(line)
  local lm = vim.fn.matchstrpos(line, c.endpoint_p)
  if lm[2] == -1 then
    return
  end
  return lm[2], lm[1]
end

local ccc = build_re(config.arrows.ascii)



local Line = { }

function Line.trace(area, r, c)
  local cs = area:connections(r, c)

  local i = 1
  if cs == 'tb' then
    while area:has_conn(r + i, c) == 'tb'

end

local LineDraw = { }

LineDraw.meta = { __index = LineDraw }
LineDraw.defaults = { }

function LineDraw:trace(area, r, c)
  local ch = area:char(r, c)
  local cs = self:connections(ch) or ''

  if cs == '' or cs == 'tblr' then
    error('invalid starting point [' .. ch .. ' = ' .. cs .. ']')
  end

  local rows = {}
  local cols = {}
  local seen = {}

  local function _search(r, c)
    local s = table.concat(pt, ',')
    local ch = area:char(r, c)
    local cs = self.connections[ch] or ''

    if cs.t then
      _search(r - 1, c)
      --table.insert(queue, {r-1,c})
    end
    if cs.b then
      _search(r - 1, c)
      table.insert(queue, {r+1,c})
    end
    if cs.l then
      table.insert(queue, {r, c-1})
    end
    if cs.r then
      table.insert(queue, {r, c+1})
    end


  end
  if cs == 'tb' then
  elseif cs == 'lr' then
  end
  if cs:find('t', 1, true) then
    (r+1, c)
  self
end

local x = Area.new()

local qx = " ╴╸╷┐╕╻╖╗╶─╾┌┬┭╓╥┱╺╼═╒┮╤╔┲╦╵┘╛│┤╡╽┧┪└┴┵├┼┽┟╁╅╘┶╧╞┾╪┢╆╈╹╜╝╿┦┩║╢╣╙╨┹┞╀╃╟╫╉╚┺╩┡╄╇╠╊╬"


local qxx = '╓╥╖╟╫╢╙╨╜╒╤╕╞╪╡╘╧╛'

end



for i = 1, 4 do
  for j = 1, 4 do
    print(x:char(i, j))
  end
end


-- nib torso corner
--            pss

-- foo -----'

-- z ----.
--       |
-- x <---+-.
--       | |
-- w <---' |
--         |
-- y ------'

--local L = dags

--local api = vim.api

--L.ns = vim.api.nvim_create_namespace('fsh.caravandags')


--L.unicode = [[
-- ▁
--▕@▏
-- ▔

--╱▔╲
--▏@▕
--╲▁╱

--▟█▙
--█@█
--▜█▛

--▗█▖
--█@█
--▝█▘

--▛▀▌
--▌@▌
--▀▀▘

--▛▀▜
--▌@▐
--▙▄▟

--▗▄▖
--▐@▌
--▝▀▘

--╭┄╮
--┊@┊
--╰╌╯

--╭─╮
--│@│
--╰─╯

--┏━┓
--┃@┃
--┗━┛

--╔═╗
--║@║
--╚═╝

--╒═╕
--│@│
--╰─╯
--'''


--'''
--╔═╤═╦═╤═╗
--║ │ ║ │ ║
--╟─┼─╫─┼─╢
--║ │ ║ │ ║
--╠═╪═╬═╪═╣
--║ │ ║ │ ║
--╟─┼─╫─┼─╢
--║ │ ║ │ ║
--╚═╧═╩═╧═╝
--'''
--🭮 🭬 	🭭 	 	🭯	⟶←←
-- 	↑ 	→ 	↓ 	↔ 	↕ 	↖ 	↗ 	↘ 	↙ 	↚ 	↛ 	↜ 	↝ 	↞ 	↟
--U+21Ax 	↠ 	↡ 	↢ 	↣ 	↤ 	↥ 	↦ 	↧ 	↨ 	↩ 	↪ 	↫ 	↬ 	↭ 	↮ 	↯
--U+21Bx 	↰ 	↱ 	↲ 	↳ 	↴ 	↵ 	↶ 	↷ 	↸ 	↹ 	↺ 	↻ 	↼ 	↽ 	↾ 	↿
--U+21Cx 	⇀ 	⇁ 	⇂ 	⇃ 	⇄ 	⇅ 	⇆ 	⇇ 	⇈ 	⇉ 	⇊ 	⇋ 	⇌ 	⇍ 	⇎ 	⇏
--U+21Dx 	⇐ 	⇑ 	⇒ 	⇓ 	⇔ 	⇕ 	⇖ 	⇗ 	⇘ 	⇙ 	⇚ 	⇛ 	⇜ 	⇝ 	⇞ 	⇟
--U+21Ex 	⇠ 	⇡ 	⇢ 	⇣
--]]


---- split string into chars
----
--function L.arrow_end(arr, s)
--  for k, v in pairs(arr.arrows) do
--    if v.ltip ==
--end

--function L.arrow_kind(arr, s)
--  local words = vim.fn.split(s)

--  if #words < 1 then
--    return nil
--  end

--  if arr:arrow_end(words[1]) then
--  end
--end



--function preserved_view(fn, ...)
--  local state = vim.fn.winsaveview()
--  -- local ok, res = pcall(fn, ...)
--  -- if not ok then
--  --   vim.pretty_print(err)
--  -- end
--  local res = fn(...)
--  vim.fn.winrestview(state)
--  return res
--end

--function save_excursion(fn, ...)
--  local wid = vim.api.nvim_get_current_win()
--  local bid = vim.api.nvim_win_get_buf(wid)
--  local pos = vim.api.nvim_win_get_cursor(wid)
--  local mark = vim.api.nvim_buf_set_extmark(bid, fsh_ns, pos[1]-1, pos[2], {})

--  -- local ok, res = pcall(fn, ...)
--  -- if not ok then
--  --   vim.pretty_print(err)
--  -- end
--  local res = fn(...)

--  if wid ~= vim.api.nvim_get_current_win() then
--    vim.api.nvim_set_current_win(wid)
--  end
--  if bid ~= vim.api.nvim_win_get_buf(wid) then
--    vim.api.nvim_win_set_buf(bid)
--  end
--  local new_pos = vim.api.nvim_buf_get_extmark_by_id(bid, fsh_ns, mark, {})
--  if not vim.tbl_isempty(new_pos) then
--    vim.api.nvim_win_set_cursor(wid, pos)
--  end
--  return res
--end

--local fsh = {}

--function fsh.max(key, arr)
--  local m = nil
--  for i, el in ipairs(arr) do
--    local v = key(el, i)
--    if i == 1 or (type(v) == 'number' and m < v) then
--      m = v
--    end
--  end
--  return m
--end

--function fsh.max_len(arr)
--  return fsh.max(function(l) return #l end, arr)
--end

--function fsh.map(fn, arr)
--  local r = {}
--  for i, el in ipairs(arr) do
--    table.insert(r, fn(el, i))
--  end
--  return r
--end

---- 67
---- START
---- 69
---- 70
---- 1
---- END
---- 3
---- 74

-- local area = {y=10}
-- local Area = {
--   new = function()
--   end,

--   __index = function(self, idx)
--     vim.pretty_print({x=self==area, idx=idx})
--   end,
-- }

-- setmetatable(area, Area)

-- local x = area[{1,2}]
-- local y = area[12]

--function fsh.line_selection()
--  local s_start = vim.fn.getpos("v")[2]
--  local s_end = vim.fn.getpos(".")[2]

--  if s_start < s_end then
--    return { s_start, s_end, reversed=false }
--  else
--    return { s_end, s_start, reversed=true }
--  end
--end

--function bool_xor(x, y)
--  return (x and not y) or (not x and y)
--end

--function outer_arrow()
--  local sel = fsh.line_selection()
--  lines = vim.api.nvim_buf_get_lines(0, sel[1] - 1, sel[2], true)

--  local maxl = fsh.max_len(lines)
--  vim.pretty_print({ maxl=maxl })

--  local new_lines = fsh.map(
--    function(l, i)
--      local w = maxl - #l + 2
--      if i == 1 or i == #lines then
--        if bool_xor(i == #lines, sel.reversed) then
--          return l .. " <" .. vim.fn["repeat"]("-", w - 1) .. (sel.reversed and "." or "'")
--        else
--          return l .. " " .. vim.fn["repeat"]("-", w) .. (sel.reversed and "'" or ".")
--        end
--      else
--        return l .. vim.fn["repeat"](" ", w + 1) .. "|"
--      end
--    end, lines)
--  vim.api.nvim_buf_set_lines(0, sel[1] - 1, sel[2], true, new_lines)
--  vim.api.nvim_input('<esc>')
--end

--setkeys({
--  { 'ø', function()
--    outer_arrow()
--  end },
--})
--    -- save_excursion(function()
--    --   local lines = vim.api.nvim_buf_line_count(0)

--    --   vim.pretty_print({lines=lines})
--    --   vim.api.nvim_win_set_cursor(0, {lines, 0})
--    --   vim.api.nvim_put({
--    --     "this is text\nnewline",
--    --     "followup",
--    --   }, 'l', true, false)
--    -- end)
--  -- end }
---- })


---- function add_comment(text, wid)
----   local bid = vim.api.nvim_win_get_buf(wid or 0)
----   local pos = vim.api.nvim_buf_line_count(bid)
---- end

---- function outline(s)
----   for line in
