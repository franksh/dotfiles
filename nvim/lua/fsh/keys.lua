
local M = {}

vim.g.mapleader = " "
vim.g.maplocalleader = "©"

-- My Norwegian Colemak:
--
-- q w f p g j l u y ø å æ
--  a r s t d h n e i o '
-- / z x c v b k m , . -

-- BASIC MOVEMENT:
--
-- Making Vim Colemak friendly is pretty simple and reasonably
-- self-contained. In the below comment QWERTY keys are referred to as
-- {keys} and Colemak keys as [keys].
--
-- First I go from the {hjkl}-style "arrow" keys to a {ijkl}-like setup
-- instead. This is for ergonomics. In my opinion it is more comfortable
-- and natural (think of the 'wasd' used in video games) than the
-- traditional {hjkl}[1]. And from there I do {ijkl} -> [unei] and then
-- recover the lost functionality of the overwritten keys, mapping them
-- onto [hkl,] (leaving [j] free).
--
-- These keys no longer have any mnemonic bearing, but they're very easy
-- to get used to.
--
-- (This whole idea was stolen from theniceboy@github.)
--
-- [1]: which is more of a historical artifact than an ergonomics
-- decision: https://www.hillelwayne.com/post/always-more-history/
--
-- "BREAKING" CHANGES:
--
-- After this shuffle:
--
-- * [k] becomes insert instead of {i}
-- * [l] becomes undo instead of {u}
-- * [,;] becomes next/previous instead of {nN}
--
-- I use a smart f/t plugin so {,;} is not needed and I'm free to use
-- those for search instead. Note also that [j]{y} has been left free
-- and unbound as a result of this.
--
-- CHANGES NEEDED ELSEWHERE:
--
-- * most notably netrw needs to be changed.
-- * things like movement in completion lists or telescope will also
-- need to be modified.
--

function M.core_keys()
  setkeys({
    silent = true,

    -- Core movement.
    {'u', 'k'},
    {'n', 'h'},
    {'e', 'j'},
    {'i', 'l'},

    -- Make shift/ctrl-movement do sane things.
    { 'U', '<c-u>zz', desc = "Move up half a page" },
    { 'N', '^', desc = "Goto start of logical line" },
    { 'E', '<c-d>zz', desc = "Move down half a page" },
    { 'I', '$', desc = "Goto end of line" },
    -- figure out something better
    -- {'<c-u>', '10<c-y>'},
    -- {'<c-e>', '10<c-e>'},

    -- Reclaim the lost functionality of {iune}.
    {'k', 'i'}, -- insert = [k]
    {'K', 'I'},
    {'l', 'u'}, -- undo = [l]
    -- L is after plugins
    {'h', 'e'}, -- end-of-word = [h]
    {'H', 'H'},

    {',', 'n'}, -- next/prev = [,]/[;]
    {';', 'N'},

    -- [j] is now a free key.
  })

  -- nvim is now Colemak-ready!

  setkeys({
    mode = 'i',
    -- C-V should be paste, C-Q for quote.
    {'<c-v>', '<c-r>'},
    {'<c-q>', '<c-v>'},
    { '<c-l>', function() vim.lsp.buf.code_action() end },
    -- jj = alternative escape.
    {'jj', '<esc>'},
  })

  setkeys({
    { '×', '<c-^>', desc = "Switch to last used buffer" },
    { '<c-y>', '"+y', desc = "Yank to clipboard" },
    { '<leader>g', vim.cmd.Neogit, desc = "Git" },
    { '<leader>k', ':bdel<cr>', desc = "Kill buffer" },
    { '<leader>ce', ":source<cr>", desc = "Eval Lua/Vim" },

    { '<leader>N', '<c-w>h', desc = "Left window" },
    { '<leader>I', '<c-w>l', desc = "Right window" },
    { '<leader>U', '<c-w>k', desc = "Up window" },
    { '<leader>E', '<c-w>j', desc = "Down window" },
    { '<leader>S', ':w<cr>', desc = 'Save' },
    { '<leader>q', ':qall<cr>', desc = 'Quit' },
    { '<leader>Q', ':wqall<cr>', desc = 'Save & Quit' },

    { '<leader>K', vim.cmd.tabclose, desc = 'Close tab' },
    -- holy fuck this is ridiculous. all i want is: pretend the user types ':e %:p:h<tab>/'. using a
    -- plain string doesn't work, nvim_feedkeys doesn't work. neither of them trigger the switch to
    -- command mode that wakes up nvim-cmp, so you just get the built-in completion. need to do an
    -- async callback so the engine gets a tick after pressing ':' to enter the command mode...?
    { '<leader>o', function()
      vim.api.nvim_feedkeys(':', 'n', false)
      local fname = vim.fn.expand('%:p:h')
      vim.schedule(function()
        vim.api.nvim_feedkeys('e ' .. fname .. '/', 'c', false)
      end)
      -- local s = vim.api.nvim_replace_termcodes(":e %:p:h<tab>", true, false, true)
      -- vim.api.nvim_feedkeys(s, 'n', false)
    end,
      desc = "Open", remap = true },

    { desc = "Clear last search",
      mode = 'n',
      silent = true,
      '<c-h>', ':let @/ = ""<cr>' },

    { desc = "Paste from clipboard",
      '<leader><c-v>', '"+p' },

    { 'gh', vim.diagnostic.open_float, desc = 'Show diagnostic' },
  })

  -- Move selected lines up/down in visual mode.
  setkeys({
    mode = 'v',
    { '<c-u>', ":m '<-2<cr>gv=gv" },
    { '<c-e>', ":m '>+1<cr>gv=gv" },
  })

  -- Tab switching.
  setkeys({
    silent = true,
    desc = 'which_key_ignore',
    {'<leader>1', '1gt'},
    {'<leader>2', '2gt'},
    {'<leader>3', '3gt'},
    {'<leader>4', '4gt'},
    {'<leader>5', '5gt'},
    {'<leader>6', '6gt'},
    {'<leader>7', '7gt'},
    {'<leader>8', '8gt'},
    {'<leader>9', '9gt'},
  })

  -- Tab nav.
  setkeys({
    desc = 'tab navigation',
    mode = 'n',
    {'TU', ':tab split<cr>'},
  })

  -- setkeys({
  --   {'TU', ':tab split<CR>'},
  --   {'Tn', ':-tabnext<CR>'},
  --   {'TI', ':+tabmove<CR>'},
  --   {'TN', ':-tabmove<CR>'},
  -- })
end


function M.afterplugins_keys()
  -- no require necessary for undotree?

  -- nvim-tree bindings.

  -- Harpoon key bindings.


  require('iswap').setup{
    keys = 'arstdhneioqwfpgjluyzxcvbkm',
    autoswap = true,
  }

  setkeys({
    { '<leader>a', vim.cmd.ISwap },
    { '<leader>A', vim.cmd.ISwapWith },
    { '<leader>x', vim.cmd.ISwapNode },
    { '<leader>X', vim.cmd.ISwapNodeWith },
  })

  local ls = require("luasnip")


  setkeys({
    mode = 'i',
    { '<c-y>', function()

      vim.print("CY!!")
      ls.expand() end },
    { '<c-b>', function() 
      vim.print("Cb!!")
      ls.jump(1) end },
    { '<m-m>', function() 
      vim.print("mm!!")
      ls.jump(1) end },
    { '<c-n>', function()
      vim.print("Cn!!")
      ls.jump(-1) end },
    { '<c-y>', function() if ls.choice_active() then ls.change_choice(1) end end },
  })

end

-- workaround for sandwich+lightspeed
setkeys({
  {'Tx', ':echo "rrrrrrrt"<cr>'},
})

setkeys({
  {'<leader>æs', '<Plug>Lightspeed_s'},
  {'<leader>æS', '<Plug>Lightspeed_S'},
  {'<leader>æT', '<Plug>Lightspeed_T'},
})


return M
