
local o = vim.opt

-- Gutter and lines.
o.number = true
o.relativenumber = true
o.cursorline = true
o.ruler = true
o.signcolumn = 'yes'
o.list = true
o.listchars = {
	tab = '| ', trail = '▫', precedes = '◀', extends = '▶',
	nbsp = '␣'
}

-- Do not unload buffers when they are hidden.
o.hidden = true

-- Automatically chdir to file locations.
-- o.autochdir = true
-- no!! this seems to mess with a lot of things

-- 2-space indent forever. I'll die on this hill.
o.expandtab = true
o.tabstop = 2
o.shiftwidth = 2
o.softtabstop = 2
o.autoindent = true

-- Format options.
o.formatoptions:remove('t')
o.formatoptions:append('q')
o.textwidth = 100
o.colorcolumn = '100' -- why string?

-- wrapmargin=10

-- General line visibility.

-- Already has sane defaults.
-- set viewoptions=cursor,folds

-- Allow executing dir-local .vimrc / .exrc ...
-- set exrc
-- ... but disallow them from running shell commands etc
-- set secure

-- Enable 24-bit colors.
o.termguicolors = true

-- Scrolling.
o.scrolloff = 6
o.sidescroll = 8
o.scroll = 8

-- Set in which-key config.
-- set timeout
-- set timeoutlen=800

-- Random.
o.updatetime = 50
vim.g.cursorhold_updatetime = 50

-- Keycodes.
-- for hydra?
-- o.timeout = true
-- o.timeoutlen = 200

o.timeout = false
o.timeoutlen = 800
o.ttimeout = true
o.ttimeoutlen = 10

-- Some other behavior.
o.wrap = true
o.backspace = { 'indent', 'eol', 'start' }
o.splitright = true
o.splitbelow = true
o.inccommand = 'split'

-- Folding?
o.foldmethod = 'indent'
o.foldlevel = 99
o.foldenable = true

o.showmode = true
o.showcmd = true
o.shortmess:append('c')

-- Show menu for completions.
o.wildmenu = true

-- Search casing.
o.ignorecase = true
o.smartcase = true

-- Always on in nvim?
o.ttyfast = true

-- Don't redraw while executing macros.
o.lazyredraw = true

-- Less annoying bell.
o.visualbell = true

-- Backup.
o.backupdir = { vim.fn.expand('~/.local/state/nvim/backup//'), '.' }
o.directory = { vim.fn.expand('~/.local/state/nvim/swap//'), '.' }
o.undodir = { vim.fn.expand('~/.local/state/nvim/undo//'), '.' }
o.backup = true
o.writebackup = true
o.swapfile = true
o.updatecount = 100
o.undofile = true
o.sessionoptions:append("localoptions")

-- Turn block visual into art mode!
o.virtualedit = 'block'

o.background = 'dark'

-- Global stuff.
--
vim.g.clever_f_highlight_timeout_ms = 1500
vim.g.clever_f_timeout_ms = 4000
vim.g.rainbow_active = 1
-- let g:airline_theme='<theme>'
-- vim.g['airline#extensions#tabline#enabled'] = 1
vim.g.EasyMotion_smartcase = 1

vim.g.rust_recommended_style = false

-- need gruvbox installed at this point, but
pcall(function()
  local ayu = require('ayu')
  ayu.setup({
    mirage = false,
    overrides = {},
  })
  ayu.colorscheme()
end)

local ft_tab_width = {
	[2] = { "html", "javascript", "markdown", "toml", "yaml", "python" },
}

for k, v in pairs(ft_tab_width) do
	vim.api.nvim_create_autocmd("FileType", {
		pattern = v,
		callback = function()
			vim.opt_local.shiftwidth = k
			vim.opt_local.softtabstop = k
			vim.opt_local.tabstop = k
		end,
	})
end

return {
	utils = require("fsh.utils"),
	keys = require("fsh.keys"),
	-- plugins = require("fsh.plugins"),
}

