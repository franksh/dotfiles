
-- configs after plugins have loaded.
--
-- only purpose of this is to have my code local instead of in NVIM/after/plugin.
--
local _q = function()
  require("mini.align").setup({
  })

  require('neogit').setup()


  -- careful: if this comes too late the hooks seem to never be executed when auto-restoring
  -- sessions?
  require("projections").setup({
  })

  -- require('telescope').setup({
  --   defaults = require('telescope.themes').get_ivy({
  --     winblend = 15,
  --     layout_config = {
  --       height = 0.8,
  --     },
  --     mappings = {
  --       i = {
  --         ["<c-h>"] = "which_key",
  --       },
  --     },
  --   }),
  -- })

  -- require('telescope').load_extension('fzf')

  -- require('telescope').load_extension('projections')
  -- vim.keymap.set("n", "<leader>p", function()
  --   vim.cmd("Telescope projections")
  -- end, { desc = "Projects" })

  -- Autostore session on VimExit
  local Session = require("projections.session")
  vim.api.nvim_create_autocmd({
    'VimLeavePre'
  }, {
    callback = function()
      Session.store(vim.loop.cwd())
    end,
  })

  -- Switch to project if vim was started in a project dir
  -- local switcher = require("projections.switcher")
  vim.api.nvim_create_autocmd({
    "VimEnter"
  }, {
    callback = function()
        if vim.fn.argc() ~= 0 then return end
        local session_info = Session.info(vim.loop.cwd())
        if session_info == nil then
          Session.restore_latest()
        else
          Session.restore(vim.loop.cwd())
        end
    end,
    desc = "Restore last session automatically",
  })

  -- require('lualine').setup {
  -- }

  -- require'lightspeed'.setup({})
  -- {
  --   ignore_case = false,
  --   exit_after_idle_msecs = { unlabeled = nil, labeled = nil },
  --   --- s/x ---
  --   jump_to_unique_chars = { safety_timeout = 400 },
  --   match_only_the_start_of_same_char_seqs = true,
  --   force_beacons_into_match_width = false,
  --   -- Display characters in a custom way in the highlighted matches.
  --   substitute_chars = { ['\r'] = '¬', },
  --   -- Leaving the appropriate list empty effectively disables "smart" mode,
  --   -- and forces auto-jump to be on or off.
  --   safe_labels = { . . . },
  --   labels = { . . . },
  --   -- These keys are captured directly by the plugin at runtime.
  --   special_keys = {
  --     next_match_group = '<space>',
  --     prev_match_group = '<tab>',
  --   },
  --   --- f/t ---
  --   limit_ft_matches = 4,
  --   repeat_ft_with_target_char = false,
  -- }

  require('fsh.keys').afterplugins_keys()
end

return function() end

