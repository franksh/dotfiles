-- Various utility stuff, because barebones Lua is awful.
--

-- I loathe that Lua functions are so non-universal by default.

-- Takes a function which should return a list (r, fn), then returns a
-- variadic function which will call fn on every argument once before
-- returning r.
--
-- See usage examples below.
function mapargs(hfn)
	return function(...)
		local arg = {...}
		local r, fn = hfn()
		for i = 1, #arg do
			fn(arg[i])
		end
		return r
	end
end

-- Merge an arbitrary number of associative tables together. Given only
-- one argument it simply makes a shallow clone.
table.merge = mapargs(function()
	local copy = {}
	return copy, function(t)
		for k, v in pairs(t) do
			copy[k] = v
		end
	end
end)

-- Concatenates together an arbitrary number of array-like tables.
--
-- Why is table.concat() taken by the stupid string concatenation?
table.concatenate = mapargs(function()
	local copy = {}
	return copy, function(t)
		for _, v in ipairs(t) do
			table.insert(copy, v)
		end
	end
end)


-- Returns the associative part of a table. Opposite of ipairs() but
-- returns a table, not an iterator.
table.kvpairs = mapargs(function()
	local copy = {}
	return copy, function(t)
		local len = #t
		for k, v in pairs(t) do
			if type(k) ~= 'number' or k < 1 or k > len then
				copy[k] = v
			end
		end
	end
end)


-- Variadic version of table.remove().
--
-- Can pop an arbitrary number of keys. Returns them as a list.
function table.pop(t, ...)
    local r = {}
		local arg = {...}
		for i = 1, #arg do
			local v = arg[i]
			table.insert(r, t[v])
			t[v] = nil
		end
		return unpack(r)
end


-- Why isn't this standard.
function bind(fn, ...)
	local arg = table.merge({...})
	return function(...)
		return fn(unpack(table.concatenate(arg, {...})))
	end
end

-- vim.inspect() to echo area.
function pp(...)
	local arg = {...}
	local list = {}
	for i, v in ipairs(arg) do
		if type(v) == 'string' then
			table.insert(list, {v, 'None'})
		else
			table.insert(list, {vim.inspect(v)})
		end
	end
	vim.api.nvim_echo(list, false, {})
end


-- Simple frontend for vim.keymap.set().
function setkeys(data)
	local mode = table.pop(data, 'mode') or ''
	local opts = table.kvpairs(data)
	for _, v in ipairs(data) do
	  local this_mode = table.pop(v, 'mode') or mode
		local this_opts = table.merge(opts, table.kvpairs(v))
		vim.keymap.set(this_mode, v[1], v[2], this_opts)
	end
end


-- Some stuff below have been stolen from ibhagwan@github
--

local M = {}


function M.shell_error()
  return vim.v.shell_error ~= 0
end

function M._echo_multiline(msg)
  for _, s in ipairs(vim.fn.split(msg, "\n")) do
    vim.cmd("echom '" .. s:gsub("'", "''") .. "'")
  end
end

function M.info(msg)
  vim.cmd("echohl Directory")
  M._echo_multiline(msg)
  vim.cmd("echohl None")
end

function M.warn(msg)
  vim.cmd("echohl WarningMsg")
  M._echo_multiline(msg)
  vim.cmd("echohl None")
end

function M.err(msg)
  vim.cmd("echohl ErrorMsg")
  M._echo_multiline(msg)
  vim.cmd("echohl None")
end


function M.git_root(cwd, noerr)
  local cmd = { "git", "rev-parse", "--show-toplevel" }
  if cwd then
    table.insert(cmd, 2, "-C")
    table.insert(cmd, 3, vim.fn.expand(cwd))
  end
  local output = vim.fn.systemlist(cmd)
  if M.shell_error() then
    if not noerr then M.info(unpack(output)) end
    return nil
  end
  return output[1]
end

git = git or {}
git.root = M.git_root

return M


