-- Dummy entry point.
--
-- See lua/fsh/keys.lua etc.
--
-- As far as I understand, load order goes something like this:
--
-- * this file
-- * the compiled plugin.lua kitchensink by Packer
-- * after/plugin/* stuff
--
-- So plugin configs need to either go in the `config` hooks of Packer, or they need to go in
-- after/plugin/*. I went for the latter, but I have no idea what is right.
--
-- This approach leads to a ton of garbage errors on a fresh un-sync'd install of nvim, I guess.
--
-- Packer config hooks made me curse too much because they can't be proper closures over local
-- variables, which turns out to be more annoying than it sounds.
local fsh = require("fsh")

fsh.keys.core_keys()

-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
  if vim.v.shell_error ~= 0 then
    vim.api.nvim_echo({
      { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
      { out, "WarningMsg" },
      { "\nPress any key to exit..." },
    }, true, {})
    vim.fn.getchar()
    os.exit(1)
  end
end
vim.opt.rtp:prepend(lazypath)

-- Make sure to setup `mapleader` and `maplocalleader` before
-- loading lazy.nvim so that mappings are correct.

-- Setup lazy.nvim
require("lazy").setup({
  spec = {
    -- import your plugins
    { import = "plugins" },
  },
  -- automatically check for plugin updates
  checker = { enabled = true, notify = false, frequency = 86400 },
})

require('fsh.postplugins')

