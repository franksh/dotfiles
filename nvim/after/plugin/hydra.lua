
-- require('which-key').setup( {
--   plugins = {
--     marks = true, -- shows a list of your marks on ' and `
--     registers = true, -- shows your registers on " in NORMAL or <C-r> in INSERT mode
--     -- the presets plugin, adds help for a bunch of default keybindings in Neovim
--     -- No actual key bindings are created
--     spelling = {
--       enabled = true, -- enabling this will show WhichKey when pressing z= to select spelling suggestions
--       suggestions = 20, -- how many suggestions should be shown in the list?
--     },
--     presets = {
--       operators = true, -- adds help for operators like d, y, ...
--       motions = false, -- adds help for motions
--       text_objects = true, -- help for text objects triggered after entering an operator
--       windows = true, -- default bindings on <c-w>
--       nav = true, -- misc bindings to work with windows
--       z = false, -- bindings for folds, spelling and others prefixed with z
--       g = true, -- bindings for prefixed with g
--     },
--   },
--   triggers_blacklist = {
--     n = { '!', '<A-w>', },
--   },
--   -- triggers = "auto", -- automatically setup triggers
--   -- -- triggers = {"<leader>"} -- or specifiy a list manually
--   -- -- list of triggers, where WhichKey should not wait for timeoutlen and show immediately
--   -- triggers_nowait = {
-- 	spacing = 2,
-- 	align = 'left',
--   ignore_missing = false,
--   hidden = { "<silent>" },
--   -- triggers = { "<leader>", "T", "s", }
-- })

--local Hydra = require('hydra')

--local hint = [[

--  F^o^cus  S^w^ap     ^ Move^
--   _u_ ↑    _U_ ↑     _<c-u>_ ↑
--   _n_ ←    _N_ ←     _<c-n>_ ← 
--   _e_ ↓    _E_ ↓     _<c-e>_ ↓ 
--   _i_ →    _I_ →     _<c-i>_ → 
--   _x_ ⟷    _X_ ⟷
  
--   ^W^idt^h^   ^H^eig^h^t  ^S^pli^t^
--   _+_ / _-_   _:_ / _._   _h_ / _v_
  
--  B^a^lance      _t_: open in new tab  
--   _=_ all       _k_: kill window
--   _|_ horiz     
--   _/_ vert      _q_ / _<esc>_: quit

--]]

--Hydra({
--  name = "Windows",
--  mode = {'n','x'},
--  body = '!',
--  config = {
--    -- buffer = bufnr,
--    color = 'amaranth',
--    invoke_on_body = true,
--    hint = {
--      border = 'rounded'
--    },
--    on_enter = function()
--      vim.cmd 'mkview'
--      --vim.cmd 'silent! %foldopen!'
--      --vim.bo.modifiable = false
--    end,
--    on_exit = function()
--      --local cursor_pos = vim.api.nvim_win_get_cursor(0)
--      vim.cmd 'loadview'
--      --vim.api.nvim_win_set_cursor(0, cursor_pos)
--      vim.cmd 'normal zv'
--    end,
--    funcs = {},
--  },
--  hint = hint,
--  heads = {
--    -- move between windows
--    { "u", "<C-w>k", desc = 'Focus ↑' },
--    { "n", "<C-w>h", desc = 'Focus ←' },
--    { "e", "<C-w>j", desc = 'Focus ↓' },
--    { "i", "<C-w>l", desc = 'Focus →' },
--    { "x", "<C-w>w", desc = "Focus ⟷" },


--    -- rotate & move windows
--    { "U", "<c-w>r", desc = "Rotate ↑" },
--    { "E", "<c-w>R", desc = "Rotate ↓" },
--    { "N", "<c-w>r", desc = "Rotate ←" },
--    { "I", "<c-w>R", desc = "Rotate →" },
--    { "X", "<c-w>x", { desc = "Swap" }, },

--    { "<c-u>", "<C-w>K", { desc = "Move ↑" }, },
--    { "<c-e>", "<C-w>J", { desc = "Move ↓" }, },
--    { "<c-n>", "<C-w>H", { desc = "Move ←" }, },
--    { "<c-i>", "<C-w>L", { desc = "Move →" }, },

--    -- split horizontally or vertically
--    { "h", "<C-w>v", desc = "Split -" },
--    { "v", "<C-w>s", desc = "Split |" },

--    -- resize windows
--    { ":", "<C-w>1+", desc = "Height +" },
--    { ".", "<C-w>1-", desc = "Height -" },
--    { "-", "<C-w>2<", desc = "Width -" },
--    { "+", "<C-w>2>", desc = "Width +" },

--    --
--    -- equalize window sizes
--    { "=", "<C-w>=" },
--    { "|", ":horizontal wincmd =<cr>", { }, },
--    { "/", ":vertical wincmd =<cr>", { }, },

--    { "t", "<c-w>T", { }, },

--    --  close active window
--    { "k", ":q<cr>", { }, },

--    -- kill this Hydra
--    { "q", nil, { exit = true, nowait = true }, { }, },
--    { "<esc>", nil, { exit = true, nowait = true }, { }, },
--  }
--})


