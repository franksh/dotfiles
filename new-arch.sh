

LINUX="
man-pages man-db mlocate bash-completion
ufw openssh ldns xz zip git lz4 tlp
bind lsof net-tools rsync
"

XORG="
xorg-server
gnome-session
xdg-dbus-proxy xdg-utils
lightdm lightdm-gtk-greeter
xorg-xev
xorg-xrandr
i3 i3status
xrandr autorandr
xsel
dunst
rofi
dmenu
scrot
feh
screenkey
peek
slop
inotify-tools
"

FONTS="
ttf-fira-mono
ttf-ubuntu-font-family
ttf-fira-code
ttf-dejavu ttf-dejavu-nerd
ttf-font-awesome
noto-fonts
noto-fonts-emoji
ttf-nerd-fonts-symbols
ttf-hack-nerd
ttf-linux-libertine
ttf-inconsolata
ttf-liberation ttf-liberation-mono-nerd
"
# aura -A nonicons-font

GUI="
firefox
kitty gnome-terminal
nautilus
mattermost-desktop
discord
vlc
"

UTILS="
graphviz whois ripgrep nmap htop gnu-netcat
wget curl strace gnuplot inxi
"

MISC="
docker reflector
"

MATH="sagemath bc calc gmp-ecm"
EDITORS="neovim emacs vim pandoc asciidoctor fzf fzy"

DEV="
npm nodejs
python python-pip rustup stack
clang autoconf cmake
gdb gperf perf
"

DESKTOP="
bluez bluez-utils
pipewire pipewire-audio pipewire-pulse pipewire-alsa pipewire-jack
pavucontrol
gnome-control-center gnome-tweaks
font-manager
"

pacman --needed -Sy $FONTS $XORG $MISC $EDITORS $DEV $LINUX $UTILS $MATH $DESKTOP $GUI

# rakudo zef crystal
# sudo pacman -S aspell aspell-en aspell-nb aspell-nn ispell words gnome-keyring
# enable lightdm
# ssh-keygen -t ed25519


# vuze:
# pacman -S git nginx neovim python python-pip ...
# groupadd franksh
# useradd -m -g franksh -G sudo franksh
# visudo
# add ~/.ssh/authorized_keys
# edit /etc/nginx/nginx.conf
# location ~ ^/~(.+?)(/.*)?$ {
# alias /home/$1/public_html$2;
# autoindex on;
# }
# reflector -a 0.5 --score 100 -c no,se,dk,nl,de,fi,* -p https --verbose -f 40
