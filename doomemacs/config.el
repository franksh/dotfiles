;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Frank S. Hestvik"
      user-mail-address "tristesse@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

(setq doom-font (font-spec :family "DejaVu Sans Mono" :size 20))
(setq doom-big-font (font-spec :family "DejaVu Sans Mono" :size 42))
(setq doom-variable-pitch-font (font-spec :family "sans" :size 20))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-ir-black)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/MEGA/org/")
(setq org-roam-directory "~/MEGA/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor ;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(setq doom-localleader-key "M-m"
      doom-localleader-alt-key "M-m")

(map!
 ;; u->k ; u becomes UP
 ;; l->u ; l becomes UNDO
 ;; k->i ; k becomes INSERT
 ;; i->l ; i becomes RIGHT
 ;; K is lost!
 :nvom "u" #'evil-previous-line
 :nv "l" #'evil-undo
 :nvom "i" #'evil-forward-char
 :nvom "U" (kbd "8u")
 :nvom "I" (kbd "$")
 :nvom "k" #'evil-insert
 :nv "K" #'evil-insert-line
 :o "k" evil-inner-text-objects-map

 ;; e->j ; e becomes DOWN
 ;; h->e ; h becomes END-OF-WORD
 ;; n->h ; n becomes LEFT
 ;; j and J are now FREE
 :nvom "e" #'evil-next-line
 :nvom "n" #'evil-backward-char
 :nvom "N" (kbd "^")
 :nvom "E" (kbd "8e")
 :nv "h" #'evil-forward-word-end
 :nv "H" #'evil-forward-WORD-end

 :i "C-n" #'evil-first-non-blank-of-visual-line
 :i "C-e" #'evil-complete-next
 :i "C-S-e" #'evil-complete-previous
 :i "C-i" #'evil-end-of-visual-line

 :nviom "M-RET" #'newline-and-indent
;; THIS IS FIXED BELOW
;; :nvom "," #'evil-ex-search-next
;; :nvom ";" #'evil-ex-search-previous
 ;; :after evil-snipe
 ;;      (:map evil-snipe-local-mode-map
 ;;       :m "," nil
 ;;       :m ";" nil)
 :nvir "C-c C-u" (lambda () (interactive) (insert "undefined"))
 )

(map!
 :nv "SPC c =" #'org-babel-execute-src-block)


;; Fuck this parens shit.
(remove-hook 'doom-first-buffer-hook #'smartparens-global-mode)

(defadvice! prompt-for-buffer (&rest _)
  :after 'evil-window-vsplit (progn
  (call-interactively #'other-window)
                               (call-interactively #'persp-switch-to-buffer)))

;; Default line scope seem stupid?
(after! evil-snipe
  (setq evil-snipe-scope 'visible))

;; Get rid of stupid autocomplete-on-enter bullshit.
(after! company
  (define-key company-active-map (kbd "RET") 'nil)
  (define-key company-active-map [return] 'nil)
  (define-key company-active-map (kbd "C-i") #'company-complete-selection)
  (define-key company-active-map (kbd "C-RET") #'company-complete-selection)
  )

; (setq-hook! flycheck-mode flycheck-checker-error-threshold 1000) ;; nimsuggest problem?
(setq-default evil-shift-width 2)
(setq-default tab-width 2)
(setq tab-width 2)
(setq c-basic-offset 2)
(setq-hook! org-mode tab-width 2 evil-shift-width 2)
(setq-hook! python-mode python-indent-offset 2 evil-shift-width 2)
(setq-hook! rustic-mode rustic-indent-offset 2)
(setq-hook! rust-mode rust-indent-offset 2)
(setq-hook! raku-mode raku-indent-offset 2)
(setq-default js-indent-level 2)
(setq-default js2-basic-offset 2)

; why is this so hard?????????
(setq evil-shift-width 2)
(setq-hook! evil-mode evil-shift-width 2)
(after! evil
  (setq evil-shift-width 2))
(after! evil
  (setq! evil-shift-width 2))

;; attempt at hack to fix weird indent guide problem in haskell-mode?
(after! highlight-indent-guides
  (highlight-indent-guides-auto-set-faces))

(use-package jinja2-mode
  :defer
  :commands jinja2-mode
  :mode (("\\.jinja2$" . jinja2-mode)
         ("\\.j2$" . jinja2-mode)
         ("\\.j2.html$" . jinja2-mode)))

(after! flycheck
  (setq-default flycheck-disabled-checkers '(python-flake8)))
(use-package flycheck
  :config
  (setq-default flycheck-disabled-checkers '(python-flake8)))

  (setq lsp-pylsp-plugins-pycodestyle-enabled nil
        lsp-pylsp-plugins-pydocstyle-enabled nil
        lsp-pylsp-plugins-flake8-enabled nil
        lsp-pylsp-plugins-pylint-enabled t
        ;; lsp-pyls-plugins-rope-completion-enabled nil ;; Disable to ensure jedi
        lsp-pylsp-configuration-sources ["pylint"]
        ;; lsp-log-io nil
        )
(after! lsp
  (setq lsp-pylsp-plugins-pycodestyle-enabled nil
        lsp-pylsp-plugins-pydocstyle-enabled nil
        lsp-pylsp-plugins-flake8-enabled nil
        lsp-pylsp-plugins-pylint-enabled t
        ;; lsp-pyls-plugins-rope-completion-enabled nil ;; Disable to ensure jedi
        lsp-pylsp-configuration-sources ["pylint"]
        ;; lsp-log-io nil
        )
  (setq! lsp-pylsp-plugins-pycodestyle-enabled nil
        lsp-pylsp-plugins-pydocstyle-enabled nil
        lsp-pylsp-plugins-flake8-enabled nil
        lsp-pylsp-plugins-pylint-enabled t
        ;; lsp-pyls-plugins-rope-completion-enabled nil ;; Disable to ensure jedi
        lsp-pylsp-configuration-sources ["pylint"]
        ;; lsp-log-io nil
        )
  (setq-default lsp-pylsp-plugins-pycodestyle-enabled nil
        lsp-pylsp-plugins-pydocstyle-enabled nil
        lsp-pylsp-plugins-flake8-enabled nil
        lsp-pylsp-plugins-pylint-enabled t
        ;; lsp-pyls-plugins-rope-completion-enabled nil ;; Disable to ensure jedi
        lsp-pylsp-configuration-sources ["pylint"]
        ;; lsp-log-io nil
        )
  )
;; (setq lsp-dart-sdk-dir "/home/franksh/snap/flutter/common/flutter/bin/cache/dart-sdk")
;; (setq lsp-dart-flutter-sdk-dir "/home/franksh/snap/flutter/common/flutter")

(after! org
  (map!
   :nv "M-Q" #'org-fill-paragraph)

  (setq org-todo-keywords
        '(
          (sequence "IDEA(i)" "TODO(t)" "PROG(p)" "|" "DONE(d)" "CANCELLED(c)")
          (sequence
           "[ ](T)"   ; A task that needs doing
           "[-](S)"   ; Task is in progress
           "[?](W)"   ; Task is being held up or paused
           "|"
           "[X](D)")  ; Task was completed
          (sequence
           "|"
           "OKAY(o)"
           "YES(y)"
           "NO(n)"))
          )
  (require 'ox-publish) ;; unnecessary?
  (require 'ox-html)

  (setq org-publish-project-alist
        '(("ctfnotes"
           :base-directory "~/onedrive/ctfnotes/"
           :base-extension "org"
           :publishing-directory "~/test/m13h.github.io/html/"
           :recursive t
           :publishing-function org-html-publish-to-html
           :section-numbers nil
           :with-date nil
           :with-author nil
           :with-timestamps nil
           :html-postamble nil
           )))
  )

(use-package! tree-sitter
  :config
  (require 'tree-sitter-langs)
  (global-tree-sitter-mode)
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))

(after! evil-snipe
  (setq evil-snipe-scope 'visible))

(setq +evil-repeat-keys (cons "," ";"))

(use-package! wsgl-mode :mode ("\\.wgsl\\'" . wgsl-mode))

;; https://github.com/hlissner/doom-emacs/commit/77fbde957a86cf7d69e9352dc1c924558a26ae7a
;; This was removed from Doom Emacs but I'd like to use it.
;; XXX: changed :after-while advice to :after, because the former didn't work. No idea why.
(defmacro set-repeater! (command next-func prev-func)
  "Makes ; and , the universal repeat-keys in evil-mode.
To change these keys see `+evil-repeat-keys'."
  `(defadvice! ,(intern (format "+evil--repeat-%s-a" (doom-unquote command))) (&rest _)
     :after #',command
     (when +evil-repeat-keys
       (evil-define-key* 'motion 'local
         (kbd (car +evil-repeat-keys)) #',next-func
         (kbd (cdr +evil-repeat-keys)) #',prev-func))))

; (evil-define-key* 'motion 'local (kbd (car +evil-repeat-keys)) #'evil-ex-search-next)
; (message "%s -- " +evil-repeat-keys)

;; n/N
(set-repeater! evil-ex-search-next evil-ex-search-next evil-ex-search-previous)
(set-repeater! evil-ex-search-previous evil-ex-search-next evil-ex-search-previous)
(set-repeater! evil-ex-search-forward evil-ex-search-next evil-ex-search-previous)
(set-repeater! evil-ex-search-backward evil-ex-search-next evil-ex-search-previous)

;; f/F/t/T/s/S
(after! evil-snipe
(setq evil-snipe-repeat-keys nil
     evil-snipe-override-evil-repeat-keys nil) ; causes problems with remapped ;
  (set-repeater! evil-snipe-f evil-snipe-repeat evil-snipe-repeat-reverse)
  (set-repeater! evil-snipe-F evil-snipe-repeat evil-snipe-repeat-reverse)
  (set-repeater! evil-snipe-t evil-snipe-repeat evil-snipe-repeat-reverse)
  (set-repeater! evil-snipe-T evil-snipe-repeat evil-snipe-repeat-reverse)
  (set-repeater! evil-snipe-s evil-snipe-repeat evil-snipe-repeat-reverse)
  (set-repeater! evil-snipe-S evil-snipe-repeat evil-snipe-repeat-reverse)
  (set-repeater! evil-snipe-x evil-snipe-repeat evil-snipe-repeat-reverse)
  (set-repeater! evil-snipe-X evil-snipe-repeat evil-snipe-repeat-reverse))

;; ;; */#
(set-repeater! evil-visualstar/begin-search-forward
               evil-ex-search-next evil-ex-search-previous)
(set-repeater! evil-visualstar/begin-search-backward
               evil-ex-search-previous evil-ex-search-next)


